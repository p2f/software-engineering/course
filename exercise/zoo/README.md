# Zoo

```plantuml
class Addetto
Addetto "1..*" --- "1" Casa : ha

class Animale {
    dataArrivo
    dataNascita
    nome
    paeseProvenienza
    sesso
}
Animale "1" -- "*" Controllo
Animale "0..1" -- "1" Gabbia : in
Animale  "1" -- "1" Genere : /di

class Area
Area "1" *--- "1..*" Casa
Area "1..*" ---* "1" Zoo

class Casa
Casa "*" o--- "1" Gabbia
Casa "*" --- "1" Genere

class Controllo {
    data
    dieta
    peso
}
Controllo "*" --- "0..1" Malattia : rileva
Controllo "*" --- "1" Veterinario

class Gabbia {
    giornoPulizia
}

class Genere

class Malattia

class Veterinario

class Zoo
```