```plantuml

@startuml

|Autori|
start
:Prepara proposta;
:Manoscritto\n[Proposta];

|Casa Ed.|
:Valutazione;
:Manoscritto\n[Proposta];

|Revisori|
:Revisione;
if () then

    |Casa Ed.|
    :Manoscritto\n[Accettato];

    |Autori|
    :Scrittura;

    |Casa Ed.|
    :Manoscritto\n[Completo];

    split

        |Casa Ed.|
        :Null;

    split again

        |Casa Ed.|
        :Ricezione e\nspedizione;

        |Tipografia|
        :Manoscritto\n[Completo];
        :Prepara bozze;
        :Manoscritto\n[Bozza];

    end split

    |Autori|
    :Correzione;
    :Manoscritto\n[Bozza corr.];

    |Casa Ed.|
    :Stampa;
    :Manoscritto\n[Copia];

    |Negozio|
    :Vendita;

else 
    |Revisori|
    stop
endif

|Negozio|
stop

@enduml

```

```plantuml

@startuml

[*] -> Proposta
Proposta -> Accettato : Invio [Ok]
Proposta --> [*] : Invio [Ko]
Accettato : do/scrittura
Accettato -> Completo
Completo : do/preparaBozza
Completo -> Bozza
Bozza : do/correzione
Bozza -> BozzaCorretta
BozzaCorretta : do/stampa
BozzaCorretta -> Copia

@enduml

```