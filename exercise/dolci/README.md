# Dolci (page 46)

## Activity diagram

```plantuml
@startuml

(*) --> ===B1===
--> "Incorporare"
--> "Sbattere"
--> [Ben fermo] ===B2===

===B1=== --> "Sciogliere"
--> ===B2===
--> "Aggiungere"
--> "Mescolare"
--> [Dopo un minuto] "Preparare palline"
--> ===B3===

===B1=== --> "Imburrare teglia"
--> ===B3===
--> "Disporre in teglia"
--> ===B4===

===B1=== --> "Accendi forno"
--> "Riscaldamento"
--> [200 gradi] ===B4===
--> "Infornare"
--> [Dopo un'ora] ===B5===

===B1=== --> "Montare"
if "Ben ferma" then
	  -->[Miele] "Dolcifico con miele"
          --> ===B5===
else
	  -->[!Miele] "Dolcifico con zucchero"
	  --> ===B5===
endif

===B5=== --> "Farcire"
--> "Disporre"
if "" then
	  -->[Miele] "Guarnisco con miele"
          --> (*)
else
	  -->[!Miele] "Guarnisco con cacao"
	  --> (*)
endif
@enduml
```
