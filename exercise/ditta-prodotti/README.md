# Ditta prodotti (page 21)

## Class diagram (project)

```plantuml
Container "1" o-- "1..*" Prodotto
Container "1" o-- "1..*" Ditta
Ditta "1" o-- "1..*" Cliente
Ditta "1" o-- "1" Magazzino
Cliente "1" o-- "*" Ordine
Ordine "1" o-- "1..*" Per
Per "*" o-- "1" Prodotto
```

**Note:** `Container -> Prodotto` is necessary, else we wouldn't be able to find all the products.