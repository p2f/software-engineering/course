# Sci

## Class diagram

```plantuml
class Impianto {
    punti
    costo biglietto
}

class Pista {
    nome
    lunghezza
    colore
}

class APunti {
    nPunti
    /puntiResidui
}

class ATempo {
    durata
    data
    datiAnagrafici
    Riduzione[y/n]
}

class Risalita {
    data
    ora
}

Comprensorio "1" *-- "1..*" Impianto
Funivia --|> Impianto
Seggiovia --|> Impianto
Ski --|> Impianto
Impianto "1..*" -- "1..*" Pista : serve
Impianto "1" -- "*" Risalita : su
Risalita "0..1" -- "0..1" Biglietto
Risalita "*" -- "0..1" Skipass : fatta con \n - OR -
APunti --|> Skipass
ATempo --|> Skipass
ATempo "*" -- "0..1" Hotel
```

## Use case diagram

```plantuml
actor sciatore
actor venditore

left to right direction

rectangle Comprensorio {
    sciatore -- (risalita)
    
    sciatore -- (acquisto\nskipass)
    (acquisto\nskipass) -- venditore
    (acquisto\nskipass) .> (pagamento\ncauzione) : <<include>>
    (acquisto\nskipass\ncon cauzione) .> (acquisto\nskipass) : <<extend>>
    
    sciatore --(acquisto\nbiglietto)
    (acquisto\nbiglietto) -- venditore
    
    sciatore -- (restituzione\nskipass)
    (restituzione\nskipass) -- venditore
    (restituzione\nskipass) .> (reso\ncauzione) : <<include>>
}
```