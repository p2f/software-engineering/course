```plantuml
@startuml
autoactivate on

C1_Client -> RS: login(username, password)

RS -> DBMS: check(username, password)

alt

RS <-- DBMS: [!found] error
C1_Client <-- RS: error

end

RS <-- DBMS: [else] ok
RS <- RS: add(C1)
RS <-- RS

C1_Client <-- RS: ok

C1_Client -> SS: (file_list)
SS -> SS: save(file_list)
return
SS <-- SS

C1_Client <-- SS

@enduml
```

```plantuml
@startuml

activate C1_Client
C1_Client -> SS: find(file_name)
activate SS
SS -> SS: find(file_name)
activate SS

alt

SS <- SS: [!found] error
C1_Client <-- SS

else

SS <- SS: [else] ok <<create>>
return
create CL_Client
SS -> CL_Client
return send(CL)

C1_Client <- C1_Client: browse(CL)
activate C1_Client
C1_Client <-- C1_Client: choice(CL)
deactivate C1_Client

end

@enduml
```

```plantuml
@startuml

activate C1_Client
C1_Client -> C2_Client: get(file_name)
activate C2_Client
C1_Client <-- C2_Client: send(file)
deactivate C2_Client
C1_Client -> CL_Client !! : <<destroy>>
deactivate C1_Client

@enduml
```