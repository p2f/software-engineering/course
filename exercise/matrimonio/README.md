```plantuml

[*] -> Celibe
Celibe : divertirsi
Celibe -> Celibe : Annull.SR [Rel] /Rel := false
Celibe --> Fidanzato : Scambio anelli
Fidanzato : do/uscire
Fidanzato --> ConiugatoCivile : MatrimonioCivile
Fidanzato -> ConiugatoCivile : MatrimonioChiesa [!Rel] \Rel = true
ConiugatoCivile : do/guardareTv
ConiugatoCivile --> Separato : Separazione
Separato -> ConiugatoCivile : Riconciliazione
Separato --> Divorziabile : After 3y
Divorziabile -> ConiugatoCivile : Riconciliazione
Divorziabile -> Celibe : Divorzio

```