```plantuml

StudioMedico "*" - "1..*" Macchinari : Ospita
StudioMedico "1" -- "1..*" Prestazione

Prestazione <|-- Visita
Prestazione <|-- Esame
Prestazione <|-- Trattamento

Prestazione "1" - "*" PrestazioneErogata : Corrisponde a

Esame "*" -- "1" Macchinari

Trattamento "*" -- "1" Macchinari : Con

Persona <|-- Medico
Persona <|-- Assistente
Persona <|-- Paziente

Medico "1" -- "*" PrestazioneErogata
Assistente "1" -- "*" PrestazioneErogata
Paziente "1" -- "1..*" PrestazioneErogata

Paziente "*" -- "1" Listino
Listino "1" o- "1..*" Esecuzione

(Prestazione, Esecuzione) .. PrestazioneEsecuzione
class PrestazioneEsecuzione {
    Quota
}

PrestazioneErogata <|-- VisitaErogata
PrestazioneErogata <|-- EsErogato
PrestazioneErogata <|-- TrErogato

TrErogato "1" *-- "1..*" Seduta : {Ordered}

VisitaErogata "1" -- "1" Terapia : Preserv.

Terapia "1..*" - "1..*" Farmaco

(Terapia, Farmaco) .. TerapiaFarmaco

class TerapiaFarmaco {
    Dose
}

```
