# Maestri-scuole (page 23)

## Answers

1. Yes
2. 1 or 2
3. 1..* (ternary)

## Class diagram (project)

```plantuml
Scuola "1" o--- "1..*" AnnoScolastico
Scuola "1" o-- "1..*" Maestro
Scuola "1" o-- "1..*" Sezione
AnnoScolastico "1" o--- "1..*" Ruolo
Ruolo "1..*" o--- "1" MaestroRuolo
Ruolo "1..*" o--- "1" Sezione
Sezione "1..*" o-- "*" Supplenza
Supplenza "*" o-- "1" AnnoScolastico
Supplente "1" o--- "*" Supplenza
Maestro <|--- Supplente
Maestro <|-- MaestroRuolo
```