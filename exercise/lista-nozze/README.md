# Lista nozze

## Class diagram

```plantuml
class Articolo {
    nome
    costo
    qtà magazzino
}

class Tipologia {
    % credito
}

class ArticoloLista {
    qtà
}

class ArticoloAcquistato {
    qtà
}

class Coppia{
    nome1
    nome2
    dataMatrimonio
    /credito
}

class ArtServComp {
    qtà
}

class ServizioComposto {
    sconto
}

Negozio "1" -- "1..*" Commesso : < lavora in
Negozio "1" -- "1..*" Articolo : vende
Articolo -- "1" Tipologia : di
Articolo <|--- ServizioComposto  : {c,d} :tipologia
Articolo <|--- ServizioBase
Articolo <|--- "Pezzo Singolo"
Articolo <|--- "Grande Elettr"
Articolo <|--- "Piccolo Elettr"
ListaNozze "1..*" o-- "*" Articolo
(ListaNozze, Articolo) .. ArticoloLista
ArticoloLista "1..*" -- "*" Acquirente
(ArticoloLista, Acquirente) .. ArticoloAcquistato
ListaNozze "*" -- "1" Commesso : resp di
ListaNozze "1" -- "1" Coppia : di
ServizioComposto "*" *-- "1..*" ServizioBase
ArtServComp .. (ServizioComposto, ServizioBase)
ServizioComposto "*" *--- "*" "Pezzo Singolo"
ServizioBase "1..*" -- "1" Tipo
Tipo "1..*" --* "1" Categoria
Acquirente "1" o-- "1..*" Persona
```

## Use case diagram

![](./se-20191004-usecase02.png)
