# Museo

```plantuml
class Museo
Museo "1" *-- "1..*" Sezione

class Sezione {
    orario
}
Sezione "1" *-- "1..*" Sala

class Turno {
    giorno
}
Turno "1..7" --- "1" Custode : < fa
Turno "7" -- "1" Sezione : di

class Opera
Opera "1..*" -----o "1" Sala
Opera "*" ----- "1" Periodo : di
Opera "*" ----- "0..1" Autore : di
Opera "0..1" -----o "*" Movimento

note "{coplete, disjoint}" as operaSpecialization
operaSpecialization -- Dipinto
operaSpecialization -- Scultura
operaSpecialization -- Arazzo
operaSpecialization -- Ceramica
Opera <|- operaSpecialization

note "{coplete, disjoint}" as sculturaSpecialization
sculturaSpecialization -- Basso
sculturaSpecialization -- Alto
sculturaSpecialization -- Statua
Scultura <|-- sculturaSpecialization
```