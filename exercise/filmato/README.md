# Filmato

## Class diagram

```plantuml
class ScenaInSeq{
    velocità
}

class Scena{
    inizio
    fine
}

class Testo {
    testo in
    testo out
}

class Filmato {
    nomeFile
}

Sequenza "1" *--- "*" ScenaInSeq
ScenaInSeq "0..1 prima" --- "0..1 dopo" ScenaInSeq
(ScenaInSeq, ScenaInSeq) .. Transizione
Transizione "*" -- "1" TipoTrans : di
ScenaInSeq "*" -- "1" Scena
Filmato "1" *-- "1..*" Scena
ScenaInSeq "*" -- "0..3" TipoEffetto
Sequenza *-- "*" Testo
Testo "*" -- "1" Modalità
```

### Alternative

```plantuml
Sequenza "1" *-- "*" ScenaInSeq : {ordered}
Sequenza "1" *-- Transizione
ScenaInSeq "1" -- Transizione : seguita da
```

## Use case diagram

```plantuml
actor Utente
actor Videocamera
actor DVD
left to right direction
rectangle Filmato {
    Utente -- (Acquisizione\nfilmato)
    (Acquisizione\nfilmato) <-- Videocamera

    Utente -- (Estrazione\nscene)

    Utente -- (Montaggio)

    Utente -- (Esportazione\nsequenza)
    (Esportazione\nsequenza) --> DVD
}
```