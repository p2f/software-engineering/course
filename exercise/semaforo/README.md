# Semaforo

## State diagram

```plantuml
state VerdeChoice <<choice>>
state Giallo : entry/reset t
state RossoEAvantiLamp : do/flash
state RossoEAvantiLamp1 : do/flash

[*] --> Rosso: /reset t
Rosso --> RossoEAvantiLamp: t=30s\n/reset t
Rosso --> Rosso1: Pressione\nPulsante
Rosso1 --> RossoEAvantiLamp: t=40s\n/reset t
RossoEAvantiLamp --> RossoEAvantiLamp1: Pressione\nPulsante\n/reset t
RossoEAvantiLamp --> Verde: t=5s/reset t
RossoEAvantiLamp1 --> Verde1 : t=5s/reset t
Verde --> VerdeChoice : Pressione\nPulsante
VerdeChoice --> Giallo: [t > 1m]
VerdeChoice --> Verde1 : [else]
Verde1 --> Giallo : t = 1m
Verde --> Giallo : t = 3m
Giallo --> Rosso : t=5s / reset t
```