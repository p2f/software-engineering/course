# Commissione (page 49)

## Activity diagram

```plantuml
@startuml
partition Commissione {
	(*) --> "Apri plico"
	-->"Verifica correttezza"
	if "" then
	  -->[Completo] "Verifica referenze"
	  if "" then
		--> [Sufficiente] "Esamina disegno generale"
	        if "" then
		  --> [Risposta] "Nomina sottocommissione"
	          --> ===B1===
		  --> ===B2===
	
		else
		  --> [!Risposta] "Giudizio1"
	          -->(*)
		endif
	  else
	        -->[!Sufficiente] "Giudizio2"
	        -->(*)  
	  endif
	else
	  -->[!Completo] "Giudizio3"
	  -->(*)  
	endif

	===B2=== --> "Giudizio idoneo"
	--> (*)
}

partition Sottocommissione_HW {
	===B1=== --> "Esame HW"
	if "" then
	  -->[Idoneo] ===B2===
	else
	  -->[!Idoneo] "Giudizio4"
	  -->(*)  
	endif
}

partition Sottocommissione_SW {
	===B1=== --> "Esame SW"
	if "" then
	  -->[Idoneo] ===B2===
	else
	  -->[!Idoneo] "Giudizio5"
	  -->(*)  
	endif
}
@enduml
```
