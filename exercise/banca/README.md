# Banca (page 18)

## Class diagram (project)

```plantuml
Banca "1" o-- "1..*" EstrattoConto
Banca "1" o-- "1..*" Conto
Banca "1" o-- "1..*" Cliente
Conto "1" o-- "*" EstrattoConto
Conto "1..*" o-- "1..*" Cliente
Cliente "1" o-- "1..*" Conto
EstrattoConto "1" o-- "*" Movimento
```

**Note:** `Conto -> Cliente` and `Banca -> Cliente` aren't associations used frequently, so if the completeness is 100% respected those associations could be deleted (this is the case).

Else if `Cliente` cardinality would have been `*` (the zero is fundamental) those associations would have been necessary.