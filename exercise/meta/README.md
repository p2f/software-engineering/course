```plantuml 
@startuml

class Classe {
    nome
    stereotipo
    abstract : bool
}

Classe "1" *- "*" Attributo

class Attributo {
    nome
    tipo
}

class Associazione {
    nome
}

class Estremo {
    ruolo
    cardinalità
    aggregazione : bool
    composizione : bool
    freccia : bool
}

Associazione "1" *-- "2..*" Estremo
Estremo "*" --- "1" Classe
note right on link
    Vincolo
end note

Classe "0..1" -- "0..1" Associazione : Classe associativa di >

class Gerarchia {
    copertura
}

Gerarchia "1..*" -- "*" Classe
Gerarchia "*" -- "1..*" Classe

@enduml
```