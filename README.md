# Software Engineering

## Useful tools

- Use Case diagrams
  - [draw.io (no templates)](https://about.draw.io/uml-use-case-diagrams-with-draw-io/)
- All in one  
  - [StarUML](http://staruml.io/)
- The choosen one  
  - [PlantUML](http://plantuml.com/)

## Index

- Use Case diagrams
  - [Exercise 1](./exercises/use_case/exercise1/se-20191001-usecase01.png)
  - [Exercise 2](./exercises/use_case/exercise2/se-20191004-usecase02.md)
