# Class diagram class notes

## Exercises methodology

1. lettura del testo per intero
2. lettura del testo una frase alla volta
3. per ogni frase riconoscere le specifiche
4. per ogni specifica, riconoscerne la classe di appartenenza (statica, dinamica, funzionale)
   - statica: struttura dell'applicazione, classi del dominio
   - dinamica: stato degli oggetti e loro cambiamento in funzione di eventi
   - funzionale: funzionalità e processi aventi input/output di dati
5. Per ogni categoria di specifica, associare il diagramma più adatto
6. decidere se il diagramma rappresenta uno storico o uno "snapshot"
   - storico: le classi rappresentano istanze nel corso del tempo
   - snapshot: le classi rappresentano istanze che esistono univocamente nel tempo
7. valutare se creare la classe contenitore
   - in un diagramma di analisi delle classi, la classe contenitore si modella solo se emerge dal testo del problema (ad esempio se viene ripetuto)
   - in un diagramma di progettazione delle classi, la classe contenitore va aggiunta obbligatoriamente poiché la sua istanza (che è univoca) permette di raggiungere tutti gli altri oggetti
8. ad ogni classe ideata o creata considerare sempre le istanze che la popolano
9. descrivere sempre un'associazione, tipicamente mediante un verbo
   - il verso di lettura di un'associazione è da sinistra verso destra, dall'alto verso il basso
   - se la disposizione delle classi nel diagramma non permette di scrivere un'associazione nel verso corretto
     allora è doveroso disegnare un triangolo nero che punta al verso di lettura corretto
10. verificare se le associazioni create formano dei cicli ridondanti (associazioni derivabili da altre associazioni),
    per farlo è possibile rimuovere una alla volta le associazioni che partecipano al ciclo e verificare che il diagramma abbia ancora senso
    - l'associazione può essere rimossa **o**
    - può essere marcata come derivata (se è importante o se viene utilizzata spesso)
      - precedendo il simbolo `/` alla descrizione dell'associazione, **e**
      - inserendo un vincolo in un elemento _post-it_

## Reificazione, classi associative e duplicazione istanze

UML, a differenza dei diagrammi E/R, le normali associazioni non presentano alcun vincolo di unicità sulle istanze di una relazione.

Ad esempio in tale diagramma è possibile avere istanze duplicate (2 persone si sposano 2 o più volte), ottenendo quindi uno storico

```plantuml
Persona "0..*" -- "0..*" Persona : sposata con
```

Tale associazione in un diagramma E/R non sarebbe stata possibile, richiedendo quindi la necessità di reificare con un'entità "Matrimonio", in cui la "dataMatrimonio" avrebbe fatto da chiave univoca e sarebbe stata condivisa nell'associazione binaria con le due persone.

In UML, tuttavia, tale vincolo compare nel caso di classe associativa: nell'esempio che segue, infatti, la duplicazione delle istanze non è più possibile.

```plantuml
Persona "0..*" -- "0..*" Persona: sposata con
(Persona, Persona) . Matrimonio

class Matrimonio {
    dataMatrimonio
}
```

Per permettere la duplicazione delle istanze nella classe reificata "Matrimonio" è dunque necessario intervenire come segue:

```plantuml
Persona "1" -- "0..*" Matrimonio
Persona "1" -- "0..*" Matrimonio

class Matrimonio {
    dataMatrimonio
}
```

Più in generale, date le due forme di reificazione che seguono, si noti che:
- le cardinalità sono le stesse
- il primo diagramma non permette la duplicazione delle istanze
- il secondo si

```plantuml
A "*" -- "*" B
(A, B) . C
```

```plantuml
A "1" -right- "*" C
C "*" -right- "1" B
```
