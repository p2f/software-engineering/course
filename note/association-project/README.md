## Association Project

Si crea un diagramma di progettazione delle associazioni a partire da un diagramma di analisi della classi fornito.

L'obiettivo è quello di dare un verso e una direzione a ciascuna associazione, rispettando sempre la logica
del diagramma delle classi e basandosi sul carico di lavoro (e non sulle cardinalità) descritto dal testo fornito.

- il diagramma di analisi delle classi fornito **non** è modificabile
- il verso delle associazioni è espresso da linee continue con frecce aperte
- l'associazione ha **sempre** semantica _part of_ (simbolo di rombo)
  - il "rombo bianco" (aggregazione) indica che all'interno di una classe vi è un riferimento ad un'altra classe
  - il "rombo nero" (composizione) indica che all'interno di una classe è presente un'altra classe (il legame è più forte)
- il diagramma di progetto delle associazioni è un'evoluzione più dettagliata di quello delle classi, pertanto
  la dipendenza può essere modellata con la scritta _<<trace>>>_ (non nei nostri esercizi)
- è possibile specificare anche un ruolo nelle associazioni (non nei nostri esercizi)
- se il carico di lavoro è bilanciato è possibile usare duplicare le associazioni, usando versi opposti
  - ciò ha un prezzo da pagare in termini prestazionali e di consistenza sui dati in caso di aggiornamento
- nelle associazioni ternarie è obbligatorio reificare aggiungendo un'altra classe
  - vincolo: la nuova classe che esprime l'associazione ternaria deve avere un ingresso da una delle classi
    che partecipano all'associazione ternaria
  - le cardinalità non sono le stesse del diagramma delle classi
    - 1 -> * in ingresso
    - * -> 1 in uscita
- in caso di classi associative
  - le associazioni devono passare per la classe associativa (non si passa mai da una classe all'altra senza
    prima passare dalla classe associativa)
  - le cardinalità vanno invertite

In sintesi: le query con maggior carico di lavoro si ottimizzano, quelle con minor carico di lavoro
sono da ottimizzare solo se strettamente necessario (ad esempio non si hanno i percorsi ottimizzati
"gratuitamente" a seguito di ottimizzazioni di query precedenti).

## Workflow

1. Riscrivere le entità, possibilmente rispettando la stessa disposizione del diagramma fornito
2. Reificare le classi ternarie, se presenti
3. Aggiungere la classe contenitore, se non presente
4. Identificare la query più frequente (criterio: maggior carico di lavoro)
5. Data la query identificata verificare se l'aggiunta di un percorso possa ridondare le associazioni
   già presenti
   - se la query in esame ha un carico di lavoro molto elevato allora può essere necessario ridondare l'associazione
     aggiungendo il percorso ideato
   - altrimenti si può evitare (buona pratica)
6. Disegnare il percorso partendo dalla classe contenitore rispettando
   la logica sul carico di lavoro espresso dal testo fornito
7. Verificare se si sono create ridondanze
8. Eliminare la query in esame, se sono soddisfatte tutte le affermazioni e il carico di lavoro
9. Se sono presenti altre query, torna al punto 4
10. Verificare che siano state modellate tutte le associazioni (tutte le classi devono essere collegate)
    - nel caso in cui una o più classi non sono state citate nelle query, allora collegarle
      nel modo opportuno (rispettando il più possibile la logica del diagramma delle classi)
11. Verificare le opzionalità: controllare che con i percorsi modellati si è in grado di raggiungere
     tutte le classi del diagramma
12. Verificare che le classi associative e le ternarie rispettino tutti i vincoli